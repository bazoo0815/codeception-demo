<?php

use \Codeception\Util\Fixtures as Fixtures;


class BlogAdminCest
{
    public function _before(AcceptanceTester $I)
    {
        Fixtures::add(
            'adminKnown',
            [
                'username' => 'anna_admin',
                'password' => 'kitten',

            ]);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    /**
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function loginAsAdmin(AcceptanceTester $I, $scenario)
    {
        $admin = Fixtures::get('adminKnown');

        $I->am('Admin');
        $I->wantTo('login into my admin account');
        $I->lookForwardTo('see admin backend');

        $I->amOnPage('/de/login');
        $I->makeScreenshot($I->createScreenshotName('login-form', $scenario));
        $I->fillField('input#username', $admin['username']);
        $I->fillField('input#password', $admin['password']);
        $I->click('Login');
        $I->amOnPage('/de/admin/post/');
        $I->canSeeLink('Logout', $I->getUrlWithPrependedHost('/de/logout'));
    }

    /**
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function testToBeSkipped(AcceptanceTester $I, $scenario)
    {
        $scenario->skip('Test is skipped for a reason');

        $I->amOnPage('/invalid-page');
        $I->see('Text that will not be shown');
    }

    /**
     * @before loginAsAdmin
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function showFirstBlogPost(AcceptanceTester $I, $scenario)
    {
        $I->amOnPage('/de/admin/post/');
        $I->makeScreenshot($I->createScreenshotName('blogpost-list', $scenario));
        $I->click('Anzeigen');
        $I->makeScreenshot($I->createScreenshotName('blogpost-single', $scenario));
        $I->canSeeCurrentUrlMatches('|^/de/admin/post/[0-9]+$|i');
    }

}
