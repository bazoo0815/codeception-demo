<?php

$I = new AcceptanceTester($scenario);
$I->am('Admin');
$I->wantTo('login into my admin account');
$I->lookForwardTo('see admin backend');

$I->amOnPage('/de/login');
$I->fillField('input#username', 'anna_admin');
$I->fillField('input#password', 'kitten');
$I->click('Login');
$I->amOnPage('/de/admin/post/');
$I->seeLink('Logout', $I->getUrlWithPrependedHost('/de/logout'));
