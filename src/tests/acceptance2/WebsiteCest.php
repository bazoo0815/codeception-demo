<?php


class WebsiteCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    /**
     * @group commonPages
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function goToHomepage(AcceptanceTester $I, $scenario)
    {
        $I = new AcceptanceTester($scenario);
        $I->am('User');
        $I->wantTo('go to the homepage');
        $I->lookForwardTo('see comapny name');

        $I->amOnPage('/');
        $I->makeScreenshot($I->createScreenshotName('home', $scenario));
    }

    /**
     * @group commonPages
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function goToJobsPage(AcceptanceTester $I, $scenario)
    {
        $I = new AcceptanceTester($scenario);
        $I->am('User');
        $I->wantTo('go to the jobs section');
        $I->lookForwardTo('see job offers');

        $I->amOnPage('/');
        $I->click('Jobs');
        $I->makeScreenshot($I->createScreenshotName('jobs', $scenario));
        $I->waitForText('Stellen');
    }

    /**
     * @param AcceptanceTester $I
     * @param $scenario
     */
    public function goToReferencesPage(AcceptanceTester $I, $scenario)
    {
        $I = new AcceptanceTester($scenario);
        $I->am('User');
        $I->wantTo('go to the references section');
        $I->lookForwardTo('see references');

        $I->amOnPage('/');
        $I->click('Referenzen');
        $I->makeScreenshot($I->createScreenshotName('references', $scenario));
        $I->waitForText('Referenzen');
    }
}
