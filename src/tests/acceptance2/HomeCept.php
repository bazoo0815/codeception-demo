<?php

$scenario->skip('Skipped');

$I = new AcceptanceTester($scenario);
$I->am('User');
$I->wantTo('go to the jobs section');
$I->lookForwardTo('see job offers');

$I->amOnPage('/');
$I->click('Jobs');
$I->makeScreenshot($I->createScreenshotName('jobs', $scenario));
$I->see('Stellen');
