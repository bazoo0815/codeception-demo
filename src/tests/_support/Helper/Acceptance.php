<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{

    public function createScreenshotName($label, $scenario = null)
    {
        $timestamp = date('Ymd-His');

        if (!is_null($scenario)) {
            $environment = $scenario->current('env');

            if (!empty($environment)) {
                $environment = '.env-'. preg_replace('/[^a-zA-Z0-9]/i', '-', $environment );
            }

            $group = $scenario->current('group');

            if (!empty($group)) {
                $group = '.group-'. preg_replace('/[^a-zA-Z0-9]/i', '-', $group );
            }
        }

        return sprintf('%s.%s%s%s', $timestamp, $label, $environment, $group );
    }

    /**
     * @return string
     * @throws \Codeception\Exception\ModuleException
     */
    public function getCurrentUrl()
    {
        return $this->getModule('WebDriver')->_getCurrentUri();
    }

    /**
     * @return string
     * @throws \Codeception\Exception\ModuleException
     */
    public function getHostUrl()
    {
        return $this->getModule('WebDriver')->_getUrl();
    }

	/**
	 * @return string
	 * @throws \Codeception\Exception\ModuleException
	 */
	public function getUrlWithPrependedHost($url)
	{
		return rtrim($this->getModule('WebDriver')->_getUrl(), '/') . $url;
	}
}
