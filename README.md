# DevTalk Codeception [2016-02-18] 
by Bernd Alter
 
## Usage of this Codeception demo 
 
A detailed description of this project and what it includes is provided in the detailed [HowTo](/howto.md)  

- [Install Codeception](#markdown-header-install-codeception)
- [Basic usage of Codeception](#markdown-header-basic-usage-of-codeception)
- [Setup Codeception for your project](#markdown-header-setup-codeception-for-your-project)
- [Commands and examples](#markdown-header-commands-and-examples)
- [Tipps & tricks](#markdown-header-tipps-tricks)
- [Helpful links](#markdown-header-helpful-links)
 
## Install Codeception

You can install Codeception by using one of the two following methods 

- simply download the Codeception phar file and add it to your project or
- **add it to your project by using composer**
 
I prefer the composer method (and use it in this demo project), because it is easier to maintain and update and there are also additional Codeception packages that you may also want to add.  
For detailed instructions on both methods see [install page on Codeception homepage](http://codeception.com/install)

**Note:**  
The following examples always refer to a composer installation of codeception.  
Paths/command calls might be slightly different when using the download method. 

## Basic usage of Codeception

After installation, there is a file `vender/bin/codecept` that you use for executing Codeception commands.  
You always add a command and optionally options and/or arguments.

```bash
vendor/bin/codecept <command> [<options>] [<arguments>]
```

To show a list of all available commands use the command `list`

```bash
vendor/bin/codecept list
```

To get help use the command `help`

```bash
vendor/bin/codecept help
```

To get help for a particular command use the command `help` followed by the command you want to get help for, e.g for the `run` command.

```bash
vendor/bin/codecept help run
```

## Setup Codeception for your project

### Initialize Codeception 

To initialize Codeception call command `bootstrap`:

```bash
vendor/bin/codecept bootstrap
```

This creates a basic folder structure with configuration files (yml) bootstrap files and several helper classes.

```bash
/tests  
    /_data                                  # folder for test data, e.g. database dumps
        dump.sql                            
    /_envs  
    /_output  
    /_support  
        /_generated
            AcceptanceTesterActions.php
            FunctionalTesterActions.php
            UnitTesterActions.php
        /Helper            
            Acceptance.php
            Functional.php
            Unit.php
    /acceptance                             # suite folder 
        _bootstrap.php 
    /functional  
        _bootstrap.php  
    /unit  
        _bootstrap.php  
    _bootstrap.php  
    acceptance.yml                          # suite configuration
    functional.yml  
    unit.yml  
```

### The test suites

Codeception generates three initial test suites (represented by folders with the same name): unit, functional and acceptance.

A suite is just a container for a group of tests.

You can create more suites at any time by using the command `generate:suite`:

```bash
vendor/bin/codecept generate:suite myCustomSuite
```

This will generate 

- the folder `tests/myCustomSuite` with a bootstrap file
- the helper class `tests/_support/Helper/MyCustomSuite.php` and 
- the configuration file `tests/myCustomSuite.suite.yml` 

## Codeception test types

- Test: unit test
- Cept: scenario-driven test
- Cest: scenario-driven, object-oriented test class


## Commands and examples

### Clean output (logs, screenshots etc.)

```bash
vendor/bin/codecept clean
```

### Generate tests

```bash
vendor/bin/codecept generate:test unit UnitTest1
vendor/bin/codecept generate:cept functional FunctionalTest1
vendor/bin/codecept generate:cest acceptance BlogFrontend
```

### Run tests

```bash
vendor/bin/codecept run acceptance
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost -vv
```

### Environments

```bash
vendor/bin/codecept generate:env myCustomEnvironment
```

#### Run tests using environments

```bash
vendor/bin/codecept run acceptance --env=ipad
vendor/bin/codecept run acceptance --env=ipad --env=chrome
vendor/bin/codecept run acceptance --env=ipad,chrome
```

```bash
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost --env=ipad
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost --env=ipad --env=iphone6
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost --env=ipad --env=chrome 
vendor/bin/codecept run tests/acceptance/BlogAdminCest.php:showFirstBlogPost --env=ipad,chrome --env=iphone6,chrome
```

### Groups 

#### Create a group
 
```bash
vendor/bin/codecept generate:group mygroup
```

#### Run tests using groups 

```bash
vendor/bin/codecept run acceptance2 --env=votum --env=votum,ipad --env=votum,iphone6
vendor/bin/codecept run acceptance2 --group=commonPages --env=siroop,ipad
```

### Generate scenarios

```bash
vendor/bin/codecept generate:scenario
```

## Tipps & tricks

- Use a test database with fixed data, because 
- Re-use code/data, do not copy/paste test snippets all the time.
- In Cest's, use method annotations like @before,@after,@group,@env  
- Acceptance tests will break often -> **Fix them!**
- Wait for elements, do not assume they are present:
  use `$I->waitForElement($context, $timeout);` (default timeout: 10 sec) instead of `$I->seeElement($context);`
- Clicks on targets are always done in the center of the target area  
Problematic case:
Click on the label of a checkbox to tick it. In the middle of the label is a clickable link, so Codeception 'clicks' on the link instead of ticking the checkbox

## Helpful links

- [Codeception commands and detailed information about their usage](http://codeception.com/docs/reference/Commands)
- [Advanced usage of Codeception](http://codeception.com/docs/07-AdvancedUsage)
- [Module WebDriver and its configuration options](http://codeception.com/docs/modules/WebDriver#Configuration)
