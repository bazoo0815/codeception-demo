# How to use this Codeception demo

## What's in here? 

This project includes the setup for a Vagrant box with 
 
- Apache2
- PHP5 with modules php5-curl, php5-intl, php5-sqlite, php5-xdebug
- Composer
- Symfony2 with [demo application](http://symfony.com/blog/introducing-the-symfony-demo-application) (a simple blog)
- Selenium standalone server (version 2.52.0)
- PhantomJS (version 2.1.1)
- Google Chrome (in headless mode)

Notes:  
Only the first module `php5-curl` is really required for Codeception, the others are needed for the Symfony demo application and debugging purposes.
  
## Requirements 
  
To use this project you need to have installed on your host 
 
- Vagrant
- Virtualbox

## Setup

To setup the Vagrant box and all the included componentes, simply run `vagrant up` and after Vagrant finished setup und provisioning ssh into the VM.  

```bash
vagrant up
vagrant ssh
```

The project folder `/src` is mounted in the VM under `/var/www/codeception/`. 

## Usage 

In order to run the Codeception tests for the Symfony demo application, you have to 

- start the server for the demo application  
```bash
cd /var/www/codeception/symfony_demo/
php app/console server:run 0.0.0.0:8000 &
```  
You can access the demo application in your browser then under [http://localhost:8000](http://localhost:8000) 

- run Selenium standalone server with headless chrome  
```bash
/home/vagrant/bin/run-selenium-server.sh --with-chrome
```

Then open another console window and run the Codeception tests

```bash
cd /var/www/codeception/
vendor/bin/codecept run 
```

Some tests will fail, but that is intended.

For more examples see section _Commands and examples_ in [README.md](/README.md)
