#!/usr/bin/env bash
# This script install PhantomJS in your Debian/Ubuntu System
#
# This script must be run as root:
# sudo bash install_phantomjs.sh
#

if [[ $EUID -ne 0 ]]; then
  SCRIPTPATH=$(readlink -f $0)
  echo -e "This script must be run as root:\nsudo -i $SCRIPTPATH" 1>&2
  exit 1
fi

# Install Chrome
CHROME_INSTALLED=$( dpkg -l | grep "google-chrome" )

if [ -z "$CHROME_INSTALLED" ]; then
  echo "Installing Google Chrome ..."

  # Add Google public key to apt
  wget -q -O - "https://dl-ssl.google.com/linux/linux_signing_key.pub" | sudo apt-key add -

  # Add Google to the apt-get source list
  APT_SRC_GOOGLE="http://dl.google.com/linux/chrome/deb/ stable main"
  CHECK_APT=$(cd /etc/apt; grep -R "$APT_SRC_GOOGLE" *)
  if [ -z "$CHECK_APT" ]; then
    echo "deb $APT_SRC_GOOGLE" >> /etc/apt/sources.list.d/google-chrome.list
  fi

  # Install Java JRE and Chrome
  apt-get update
  apt-get -y install openjdk-7-jre google-chrome-stable
else
  echo "Google Chrome is already installed."
fi

# Download and install Chrome driver
CHROMEDRIVER_VERSION="2.27"
CHROMEDRIVER_BIN="/usr/bin/chromedriver"
CHROMEDRIVER_INSTALLED=$( [ -f $CHROMEDRIVER_BIN ] && $CHROMEDRIVER_BIN -v | grep "ChromeDriver $CHROMEDRIVER_VERSION" )

if [ -z "$CHROMEDRIVER_INSTALLED" ]; then
  echo "Installing ChromeDriver ..."

  CHROMEDRIVER_FILENAME="chromedriver_linux64.zip"
  CHROMEDRIVER_DOWNLOAD_URL="http://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/$CHROMEDRIVER_FILENAME"
  CHROMEDRIVER_TARGETPATH="/tmp/$CHROMEDRIVER_FILENAME"

  apt-get -y install unzip
  wget $CHROMEDRIVER_DOWNLOAD_URL -O $CHROMEDRIVER_TARGETPATH

  if [ -f "$CHROMEDRIVER_TARGETPATH" ]; then
    unzip $CHROMEDRIVER_TARGETPATH -d /usr/bin/
    chmod 755 $CHROMEDRIVER_BIN
  else
    echo "Downloading ChromeDriver package failed. Deb file $CHROMEDRIVER_TARGETPATH is missing."
    exit 1
  fi
else
  echo "ChromeDriver (version $CHROMEDRIVER_VERSION) is already installed."
fi

# Install required X Virtual Framebuffer and related fonts
if [ -z "$(dpkg -l | grep xvfb)" ]; then
    apt-get install -y xvfb x11-xkb-utils
    apt-get install -y xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic
else
  echo "X Virtual Framebuffer is already installed."
fi
