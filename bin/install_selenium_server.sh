#!/usr/bin/env bash
# This script install PhantomJS in your Debian/Ubuntu System
#
# This script must be run as root:
# sudo bash install_seleniumserver.sh
#

if [[ $EUID -ne 0 ]]; then
	echo -e "This script must be run as root:\nsudo bash $(basename $0)" 1>&2
	exit 1
fi

SELENIUM_VERSION="3.3.1"
SELENIUM_MAJOR_VERSION=${SELENIUM_VERSION%.*}
SELENIUM_FILENAME="selenium-server-standalone-$SELENIUM_VERSION.jar"

if [ -f "/usr/local/bin/$SELENIUM_FILENAME" ]; then
  echo "Selenium standalone server (version $SELENIUM_VERSION) already installed"
  exit 0
fi

wget http://selenium-release.storage.googleapis.com/$SELENIUM_MAJOR_VERSION/$SELENIUM_FILENAME -O /tmp/$SELENIUM_FILENAME

mv /tmp/$SELENIUM_FILENAME /usr/local/bin
ln -sf /usr/local/bin/$SELENIUM_FILENAME /usr/local/bin/selenium-server-standalone.jar
