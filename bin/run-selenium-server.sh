#!/usr/bin/env bash
# This script starts Selenium server
#

RUN_CHROME=0

if test $# = 1; then
    case "$1" in
      --help | --hel | --he | --h | -h )
        echo -e "Usage: $(basename $0) [--help|-h] [--with-chrome|-c]\n\t--with-chrome|-c\tRuns Google Chrome as headless browser in background"
        exit 0
        ;;
      --with-chrome | -c )
        RUN_CHROME=1
        ;;
    esac
fi

# Check requirements
SELENIUM_SERVER="/usr/local/bin/selenium-server-standalone.jar"
if [ ! -L $SELENIUM_SERVER ]; then
    echo -e "\n  ERROR:\n   Selenium server seems not to be available (expected JAR file: $SELENIUM_SERVER). This is required.\n  Download Selenium server from here:\n   http://selenium-release.storage.googleapis.com/index.html\n"
    exit 1
fi

if [ $RUN_CHROME == 1 ]; then

    XVFB=$(which Xvfb | grep "/")
    if [ -z $XVFB ]; then
        echo -e "\n  ERROR:\n   Xvfb seems not to be installed. This is required for running Chrome in headless mode.\n  Run:\n   sudo apt-get install -y xvfb x11-xkb-utils xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic\n"
        exit 1
    fi

    CHROME=$(which google-chrome | grep "/")
    if [ -z $CHROME ]; then
        echo -e "\n  ERROR:\n   Google Chrome seems not to be installed. This is required.\n  Download and install Google Chrome for Linux from here:\n   https://www.google.com/chrome/browser/desktop/index.html\n"
        exit 1
    fi

    export DISPLAY=:10

    echo "Starting Xvfb ..."
    Xvfb :10 -screen 0 1366x768x24 -ac +extension RANDR &

    echo "Starting Google Chrome ..."
    google-chrome --no-sandbox --remote-debugging-port=9222 &

fi

echo "Starting Selenium ..."
java -jar $SELENIUM_SERVER
