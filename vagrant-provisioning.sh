#!/usr/bin/env bash

APT_UPDATE_FLAG="/home/vagrant/.apt-get-update-done"
if [ ! -f "$APT_UPDATE_FLAG" ]; then
    sudo apt-get update
    sudo apt-get install -y curl
    touch "$APT_UPDATE_FLAG"
fi

# install apache
APACHE_INSTALLED=$(dpkg -l | grep apache2)
if [ -z "$APACHE_INSTALLED" ]; then
    sudo apt-get install -y apache2
    sudo usermod -a -G www-data vagrant
else
    echo "Apache is already installed"
fi

# install php
PHP_INSTALLED=$(dpkg -l | grep php5)
if [ -z "$PHP_INSTALLED" ]; then
    sudo apt-get install -y php5 php5-intl php5-sqlite php5-curl php5-xdebug
else
    echo "PHP is already installed"
fi

# install composer globally
if [ ! -f "/usr/local/bin/composer" ]; then
    php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
    php -r "if (hash('SHA384', file_get_contents('composer-setup.php')) === 'fd26ce67e3b237fffd5e5544b45b0d92c41a4afe3e3f778e942e43ce6be197b9cdc7c251dcde6e2a52297ea269370680') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); }"
    php composer-setup.php
    php -r "unlink('composer-setup.php');"

    sudo mv composer.phar /usr/local/bin/composer
else
    echo "Composer is already installed"
fi

# rewrite apache default vhost
APACHE_DEFAULT_HOST="/etc/apache2/sites-available/000-default.conf"
DOCROOT_OLD="/var/www/html"
DOCROOT="/var/www/codeception"
VHOST_MODIFIED=$(grep "$DOCROOT" $APACHE_DEFAULT_HOST)

if [ -z "$VHOST_MODIFIED" ]; then
    sed -i -e "s|DocumentRoot $DOCROOT_OLD|DocumentRoot $DOCROOT|g" $APACHE_DEFAULT_HOST
    sudo service apache2 reload
else
    echo "Apache default vhost is already modified"
fi

# add php settings
PHP_INIS="/etc/php5/apache2/php.ini /etc/php5/cli/php.ini"
DATETIME_ZONE="date.timezone = \"Europe/Berlin\""

for PHP_INI in $PHP_INIS; do
    if [ -z "$(grep "$DATETIME_ZONE" $PHP_INI)" ]; then
        sudo echo "$DATETIME_ZONE" >> $PHP_INI
    fi
done
sudo service apache2 reload

# run project initialisation
COMPOSER_INSTALL_FLAG="/home/vagrant/.composer-install-done"
if [ ! -f "$COMPOSER_INSTALL_FLAG" ]; then
    cd $DOCROOT
    composer install

    touch "$COMPOSER_INSTALL_FLAG"
fi

# install symfony and symfony demo application
SYMFONY_BIN="/usr/local/bin/symfony"
if [ ! -f "$SYMFONY_BIN" ]; then
    sudo curl -LsS https://symfony.com/installer -o "$SYMFONY_BIN"
    sudo chmod a+x $SYMFONY_BIN

    # install symfony demo application
    if [ ! -d "$DOCROOT/symfony_demo" ]; then
        cd $DOCROOT
        symfony demo
    fi
else
    echo "Symfony is already installed"
fi
